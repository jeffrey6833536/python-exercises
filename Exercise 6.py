"""Write a program that:
A. Runs until the user guesses a number (hint: while loop)
B. Generates a random number between 1 and 9 (including 1 and 9)
C. Asks the user to guess the number
D. Then prints a message to the user, whether they guessed too low, too high
E. if the user guesses the number right, print out YOU WON! and exit the program"""

from random import randint

while True:
    random_number = randint(1, 10)
    user_guess = int(input("Please guess a number between 1 and 10: "))
    if user_guess <= random_number:
        print("You guessed too low, try again please")
    elif user_guess >= random_number:
        print("You guessed too high, try again please")
    elif user_guess == random_number:
        print("YOU WON!")
        break
