from person_data import Person


class Student(Person):
    def __init__(self, first_name, last_name, age, lectures):
        super().__init__(first_name, last_name, age)
        self.lectures = lectures

    def list_lectures_student_attends(self):
        print("Attends the lecture:")
        for lecture in self.lectures:
            print(f" - {lecture.name}")

    def add_new_lectures(self, new_lecture):
        self.lectures.append(new_lecture)

    def remove_lecture(self, lecture):
        self.lectures.pop(lecture)
