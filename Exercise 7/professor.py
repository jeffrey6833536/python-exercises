from person_data import Person


class Professor(Person):
    def __init__(self, first_name, last_name, age, subjects):
        super().__init__(first_name, last_name, age)
        self.subjects = subjects

    def list_of_subjects_taught(self):
        print("Teaches subjects:")
        for subject in self.subjects:
            print(f"- {subject.name}")

    def add_new_subject(self, new_subject):
        self.subjects.append(new_subject)

    def remove_subject(self, subject):
        self.subjects.pop(subject)

