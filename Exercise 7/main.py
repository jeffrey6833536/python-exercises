from student import Student
from professor import Professor
from lecture import Lecture

computer_science_lecture = Lecture("Computer Science", 12, 30, [])
python_lecture = Lecture("Python", 25, 60, [])
logic_reasoning_lecture = Lecture("Logic Reasoning", 10, 45, [])
python_advanced_lecture = Lecture("Python Advanced", 18, 35, [])

first_professor = Professor("James", "Zion", 35, [computer_science_lecture, python_advanced_lecture])
second_professor = Professor("Jennifer", "Mary", 39, [logic_reasoning_lecture, python_lecture])

first_professor.print_full_name()
second_professor.print_full_name()
first_professor.add_new_subject(logic_reasoning_lecture)
second_professor.add_new_subject(computer_science_lecture)
first_professor.list_of_subjects_taught()
second_professor.list_of_subjects_taught()
first_professor.list_of_subjects_taught()
second_professor.list_of_subjects_taught()

print("-----------------------------------------")

first_student = Student("Jeff", "Kikanme", 29, [computer_science_lecture, logic_reasoning_lecture])
second_student = Student("Mary", "Jane", 27, [python_lecture, python_advanced_lecture])

first_student.print_full_name()
second_student.print_full_name()
first_student.add_new_lectures(python_lecture)
second_student.add_new_lectures(computer_science_lecture)
first_student.list_lectures_student_attends()
second_student.list_lectures_student_attends()
first_student.list_lectures_student_attends()
second_student.list_lectures_student_attends()

print("-----------------------------------------")

computer_science_lecture.print_name_and_duration_of_lecture()
python_advanced_lecture.print_name_and_duration_of_lecture()
