class Lecture:
    def __init__(self, name, max_students, duration, professors):
        self.name = name
        self.max_students = max_students
        self.duration_of_lecture = duration
        self.professors_giving_lecture = professors

    def print_name_and_duration_of_lecture(self):
        print(f"{self.name} - {self.duration_of_lecture} minutes")

    def add_new_professor(self, new_professor):
        self.professors_giving_lecture.append(new_professor)
