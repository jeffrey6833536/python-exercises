

"""A. Write a program that prints out all the elements 
of the list that are higher than or equal to 10"""
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]
for number in my_list:
    if number >= 10:
        print(number)

"""B. Instead of printing the elements one by one, make a new list that has all the elements 
higher than or equal to 10 from this list in it and print out this new list."""
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]
my_new_list = []
for number in my_list:
    if number >= 10:
        my_new_list.append(number)
print(my_new_list)

"""C. Ask the user for a number as input and print a list that contains only those elements 
from my_list that are higher than the number given by the user."""

user_input = input("Please, enter a number of your choice: ")
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]
my_new_list = []
for number in my_list:
    if number > int(user_input):
        my_new_list.append(number)
print(my_new_list)
