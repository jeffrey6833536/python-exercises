employee = {
  "name": "Tim",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer"
}
# 1. Write a Python Script that:

"""A. Updates the job to Software Engineer"""
employee["job"] = "Software Engineer"
print(employee)

"""B. Removes the age key from the dictionary"""
employee.pop("age")
print(employee)

"""C. Loops through the dictionary and prints the key:value pairs one by one"""
original_employee = {
  "name": "Tim",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer"
}
for key, value in original_employee.items():
    print(f"Key: {key}, Value: {value}")

#  2.  Write a Python Script that:
dict_one = {'a': 100, 'b': 400}
dict_two = {'x': 300, 'y': 200}

"""A. Merges these two Python dictionaries into 1 new dictionary."""
merged_dict = dict_one.copy()  # Make a copy of dict_one to avoid modifying it
for key, value in dict_two.items():
    merged_dict[key] = value
print(merged_dict)

"""B. Sums up all the values in the new dictionary and prints it out"""
total_sum = sum(merged_dict.values())
print(f"The sum of all values in the merged dictionary is: {total_sum}")

"""C. Prints the max and minimum values of the dictionary"""
merged_dict_values = []
for value in merged_dict.values():
    merged_dict_values.append(value)
merged_dict_values.sort()
print(f"min value of dictionary is: {merged_dict_values[0]}")
print(f"max value of dictionary is: {merged_dict_values[len(merged_dict_values)-1]}")
