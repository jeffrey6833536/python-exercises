import openpyxl
from operator import itemgetter

employees_file = openpyxl.load_workbook("employees.xlsx")
employees_list = employees_file["Sheet1"]


# Now we delete the columns 3 and 4
employees_list.delete_cols(3, 4)

employees_by_experience = []

for row in range(2, employees_list.max_row + 1):
    employee_name = employees_list.cell(row, 1).value
    employee_experience = employees_list.cell(row, 2).value

    employees_by_experience.append({
        "name": employee_name,
        "experience": employee_experience
    })

new_list = sorted(employees_by_experience, key=itemgetter("experience"), reverse=True)

for row in range(2, employees_list.max_row + 1):
    employee_name = employees_list.cell(row, 1)
    employee_experience = employees_list.cell(row, 2)

    index = row - 2
    employee = new_list[index]

    employee_name.value = employee["name"]
    employee_experience.value = employee["experience"]

employees_file.save("employees_sorted_by_experience.xlsx")
