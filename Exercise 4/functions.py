"""A. Write a function that accepts a list of dictionaries with employee age (see example list from Exercise 3)
and prints out the name and age of the youngest employee."""


def print_employee_info(employees):
    youngest_employee_age = employees[0]['age']
    youngest_employee_name = employees[0]['name']
    for employee in employees:
        if employee['age'] < youngest_employee_age:
            youngest_employee_age = employee['age']
            youngest_employee_name = employee['name']
    print(f"The age of the youngest employee is: {youngest_employee_age}")
    print(f"The name of the youngest employee is: {youngest_employee_name}")


"""B. Write a function that accepts a string and calculates the 
number of upper case letters and lower case letters."""


def count_number_of_upper_and_lower_cases(string):
    character_list = list(string)
    lower_letters = 0
    upper_letters = 0
    for character in character_list:
        if character.islower():
            lower_letters += 1
        elif character.isupper():
            upper_letters += 1
    print(f"The number of Lower case letters is: {lower_letters}")
    print(f"The number of Upper case letters is: {upper_letters}")


"""C. Write a function that prints the even numbers from a provided list."""


def even_numbers_in_list(numbers_list):
    for number in numbers_list:
        if number % 2 == 0:  # This checks if there is a remainder after a number has been divided by 2
            print(number)
