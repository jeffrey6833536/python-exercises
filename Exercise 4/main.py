from functions import print_employee_info, count_number_of_upper_and_lower_cases, even_numbers_in_list
employees = [{
  "name": "Tina",
  "age": 30,
  "birthday": "1990-03-10",
  "job": "DevOps Engineer",
  "address": {
    "city": "New York",
    "country": "USA"
  }
},
{
  "name": "Tim",
  "age": 35,
  "birthday": "1985-02-21",
  "job": "Developer",
  "address": {
    "city": "Sydney",
    "country": "Australia"
  }
}]

numbers_input = input("Please give a list of even and odd numbers separated by ,: ")
numbers_list = list(map(int, numbers_input.split(",")))
word = input("Please give a word that comprises of upper and lower cases: ")
# A.
print_employee_info(employees)
count_number_of_upper_and_lower_cases(word)
even_numbers_in_list(numbers_list)

