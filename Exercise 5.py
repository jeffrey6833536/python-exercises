"""Write a simple calculator program that:
A. takes user input of 2 numbers and operation to execute
B. Handles the following operations: plus, minus, multiply, divide
C. Does proper user validation and give feedback: only numbers allowed
D. Keeps the Calculator program running until the user types “exit”
E. Keeps track of how many calculations the user has taken, and when the user exits the calculator program,
prints out the number of calculations the user did"""


def calculator_program(num1, num2, operation):
    if operation == "plus":
        print(num1 + num2)
    elif operation == "minus":
        print(num1 - num2)
    elif operation == "multiply":
        print(num1 * num2)
    elif operation == "divide":
        print(num1 / num2)


number_of_calculations_done = 0
while True:
    num1 = input("Please enter the first digit: ")

    if num1 == "exit":
        print("exiting calculator program")
        print(f"You have made {number_of_calculations_done} calculations")
        break

    num2 = input("Please enter the second digit: ")
    operation = input("Please enter the operator (i.e plus minus multiply divide): ")
    validate_number = num1.isnumeric() and num2.isnumeric()
    validate_operator = operation == "plus" or operation == "minus" or operation == "multiply" or operation == "divide"
    if not validate_number:
        print("only numbers allowed please")
    elif not validate_operator:
        print("Operator not valid")
    else:
        calculator_program(int(num1), int(num2), operation)
        number_of_calculations_done += 1
