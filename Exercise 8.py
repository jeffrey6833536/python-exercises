"""Write a program that:
+ accepts user's birthday as input
+ and calculates how many days, hours and minutes are remaining till the birthday
+ prints out the result as a message to the user"""

from datetime import datetime

birthday_input = input("Enter your birthday date here please (In this format DD/MM/YYYY): ")
birthday_date = datetime.strptime(birthday_input, "%d/%m/%Y").date()
today_date = datetime.today()

present_year_birthday_date = datetime(today_date.year, birthday_date.month, birthday_date.day)
future_year_birthday_date = datetime(today_date.year + 1, birthday_date.month, birthday_date.day)

days_till_birthday = 0
if present_year_birthday_date > today_date:
    days_till_birthday = present_year_birthday_date - today_date
else:
    days_till_birthday = future_year_birthday_date - today_date

print(f"There is {days_till_birthday.days} days till th birthday")
